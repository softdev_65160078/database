/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.newgen.dbproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Chanon
 */
public class DeleteDB {
    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";
        //Connect Database

        try {
            // db parameters

            // create a connection to the database
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite");

        } catch (java.sql.SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        //work space

        String sql = "DELETE FROM category WHERE category_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
           stmt.setInt(1, 7);
            int status = stmt.executeUpdate();


          

        } catch (SQLException ex) {
           System.out.println(ex.getMessage());
        }

        //Close Database
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }
}
