/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.newgen.dbproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Phattharaphon
 */
public class ConnectDB {

        public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";
        try {
            // db parameters

            // create a connection to the database
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite");
        } catch (java.sql.SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if( conn!= null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
            
        }
    }
}

