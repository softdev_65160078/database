package com.newgen.dbproject;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Chanon
 */
public class InsertDB {
     public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";
        //Connect Database

        try {
            // db parameters

            // create a connection to the database
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite");

        } catch (java.sql.SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        //work space

        String sql = "INSERT INTO category(category_in,category_name) VALUES(?,?);";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
           stmt.setInt(1, 3);
           stmt.setString(2, "candy");
            int status = stmt.executeUpdate(sql);


          

        } catch (SQLException ex) {
           System.out.println(ex.getMessage());
        }

        //Close Database
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }
}
